include_recipe "windows"
include_recipe "7-zip"

case node["deploy_env"]
when 'int'
  db="int"
when 'prod'
  db="prod"
when 'test'
  db="test"
else
  db="ci"
end

powershell "Stop Sites before deploying a new release" do
  code <<-EOH
    Import-Module WebAdministration
    $iisDefaultSiteName = "Default Web Site"
    $iisSiteName = "Workflows"

    #navigate to the sites root
    cd IIS:/Sites/
    
    #check site if exists check state and stop if started
    if (Test-Path $iisDefaultSiteName -pathType container) {
      if((Get-WebsiteState -Name $iisDefaultSiteName).Value -eq "Started") {
        Stop-Website $iisDefaultSiteName
        "STATES OF SITE: "
        (Get-WebsiteState -Name $iisDefaultSiteName).Value
      } 
    }

    if (Test-Path $iisSiteName -pathType container) {
      if((Get-WebsiteState -Name $iisSiteName).Value -eq "Started") {
        Stop-Website $iisSiteName
        "STATES OF SITE: "
        (Get-WebsiteState -Name $iisSiteName).Value
      } 
    }     
  EOH
end

inetpub="C:\\inetpub\\wwwroot\\Workflows\\services"
querygridtemp="C:\\inetpub\\wwwroot\\Workflows\\services\\QueryGrid.Workflow.Services"
renameto="QueryGrid"
querygridfolder="C:\\inetpub\\wwwroot\\Workflows\\services\\QueryGrid"

directory "C:\\inetpub\\wwwroot\\Workflows"
directory inetpub

#Delete previous build
directory querygridfolder do
  action :delete
  recursive true
end

#Delete previous build archive
file "#{inetpub}/Workflows.zip" do
  backup false
  action :delete
end

#Download the last successful build from build-machine
remote_file "#{inetpub}/Workflows.zip" do
  source "http://10.145.93.198/query-grid-mw/lastSuccessfulBuild/Workflows.zip"
end

batch "Wait 20 seconds before create" do
  code <<-EOH
    ping 127.0.0.1 -n 20 > nul
  EOH
end

#directory querygridfolder

#Unzip the last successful build from build-machine
batch "Unzip Workflows.zip" do
  code <<-EOH
    IF EXIST "C:\\Program Files\\7-Zip\\7z.exe" ("c:\\Program Files\\7-Zip\\7z" x #{inetpub}\\Workflows.zip -o#{inetpub} -r -y)
    IF EXIST "C:\\7-Zip\\7z.exe" ("c:\\7-Zip\\7z" x #{inetpub}\\Workflows.zip -o#{inetpub} -r -y)
    IF EXIST "C:\\Program Files (x86)\\7-Zip\\7z.exe" ("c:\\Program Files\\7-Zip\\7z" x #{inetpub}\\Workflows.zip -o#{inetpub} -r -y)
    ping 127.0.0.1 -n 20 > nul
  EOH
end

powershell "Rename 'QueryGrid.Workflow.Services' to 'Workflows'" do
  code <<-EOH
    ren #{querygridtemp} #{renameto}
  EOH
end

#Modify Web.config file
powershell "Modify Web.config" do
  code <<-EOH
  
  $configPath = "C:\\inetpub\\wwwroot\\Workflows\\services\\QueryGrid\\Web.config"
  $configPathTemp = "C:\\inetpub\\wwwroot\\Workflows\\services\\QueryGrid\\Web.temp"
  
  $replaceWhat = "http://localhost:25112"
  $replaceWithInt = "http://querygrid-int-backend.nibr.novartis.net/services/QueryGrid"
  $replaceWithProd = "http://querygrid-backend.nibr.novartis.net/services/QueryGrid"
  $replaceWithTest = "http://querygrid-test-backend.nibr.novartis.net/services/QueryGrid"
  
  $r1 = "http://int-aeos.eu.novartis.net:8080"
  $r1t = "http://test-aeos.eu.novartis.net:8080"
  $r2 = "http://prod-aeos.eu.novartis.net:8080"
  $r3 = "http://chbs-connect-int.eu.novartis.net"
  $r3t = "http://chbs-connect-test.eu.novartis.net"
  $r4 = "http://chbs-connect.eu.novartis.net"
  
  $r5 = "http://nrchbs-s4130.nibr.novartis.net/services/SMF/Lookup/LookupService.svc"
  $r6 = "http://nrchbs-s4552.nibr.novartis.net/services/SMF/Lookup/LookupService.svc"
  $r5t = "http://nrchbs-s4129.nibr.novartis.net/services/SMF/Lookup/LookupService.svc"

  
  $r7 = "http://int-chronos.eu.novartis.net:8080/chronos/inventory"
  $r7t = "http://test-chronos.eu.novartis.net:8080/chronos/inventory"
  $r8 = "http://prod-chronos.eu.novartis.net:8080/chronos/inventory"
  
  
  $r9 = "http://localhost:63448/QueryGridCallBackService.svc"
  $r9t = "http://nrchbs-swd0006.nibr.novartis.net/apps/QueryGrid/QueryGridCallBackService.svc"
  $r9i = "http://nrchbs-swt0006.nibr.novartis.net/apps/QueryGrid/QueryGridCallBackService.svc"
  $r9p =  "http://querygrid-ui-backend-chbs.nibr.novartis.net/apps/QueryGrid/QueryGridCallBackService.svc" 
  
  $env = "#{db}"
  if ($env -eq "int")
    {   
      (Get-Content $configPath) | 
      Foreach-Object{$_ -replace "$($replaceWhat)", "$($replaceWithInt)"} |
      Foreach-Object{$_ -replace "$($r9)", "$($r9i)"} |       
      Set-Content $configPathTemp
    
      Remove-Item $configPath
      ren $configPathTemp "Web.config"
    }
    
    if ($env -eq "prod")
    {
      (Get-Content $configPath) | 
      Foreach-Object{$_ -replace "$($replaceWhat)", "$($replaceWithProd)"} | 
      Foreach-Object{$_ -replace "$($r1)", "$($r2)"} | 
      Foreach-Object{$_ -replace "$($r3)", "$($r4)"} | 
      Foreach-Object{$_ -replace "$($r5)", "$($r6)"} | 
      Foreach-Object{$_ -replace "$($r7)", "$($r8)"} |
      Foreach-Object{$_ -replace "$($r9)", "$($r9p)"} | 
      Set-Content $configPathTemp
    
      Remove-Item $configPath
      ren $configPathTemp "Web.config"
    }
    
    if ($env -eq "test")
    {
      (Get-Content $configPath) | 
      Foreach-Object{$_ -replace "$($replaceWhat)", "$($replaceWithTest)"} | 
      Foreach-Object{$_ -replace "$($r1)", "$($r1t)"} | 
      Foreach-Object{$_ -replace "$($r3)", "$($r3t)"} | 
      Foreach-Object{$_ -replace "$($r5)", "$($r5t)"} | 
      Foreach-Object{$_ -replace "$($r7)", "$($r7t)"} | 
      Foreach-Object{$_ -replace "$($r9)", "$($r9t)"} | 
      Set-Content $configPathTemp
    
      Remove-Item $configPath
      ren $configPathTemp "Web.config"
    }
   
  EOH
end

powershell "Create web app and app pool for Workflows" do
  code <<-EOH
    Import-Module WebAdministration
    $iisAppPoolName = "QueryGridAppPool"
    $iisAppPoolDotNetVersion = "v4.0"
    
    $iisAppName = "Workflows"
    $iisAppsPath = "Workflows/services"
    $iisAppsName = "services"
    $iisQueryGridPath = "Workflows/services/QueryGrid"
    $iisQueryGridName = "QueryGrid"
    $directoryPath = "C:\\inetpub\\wwwroot\\Workflows\\services\\QueryGrid"
    $iisPort = "80"

    #navigate to the app pools root
    cd IIS:/AppPools/

    #check if the app pool exists
    if (!(Test-Path $iisAppPoolName -pathType container))
    {
        #create the app pool
        $appPool = New-Item $iisAppPoolName
        $appPool | Set-ItemProperty -Name "managedRuntimeVersion" -Value $iisAppPoolDotNetVersion
        $appPool | Set-ItemProperty -Name "enable32BitAppOnWin64" -Value "true" 
    }

    #navigate to the sites root
    cd IIS:/Sites/

    #check if the SITE exists
    if (!(Test-Path $iisAppName -pathType container))
    {
      #create the site
      $iisApp = New-Item $iisAppName -bindings @{protocol="http";bindingInformation=":" + $iisPort + ":"}
    }
    
    #check if the DIRECTORY exists
    if (!(Test-Path $iisAppsPath -pathType container))
    {
      #create the directory
      cd $iisAppName
      $iisAppsDir = New-Item $iisAppsName -type directory
      cd ..
    }
    
    #check if the APPLICATION exists
    if (!(Test-Path $iisQueryGridPath -pathType container))
    {
      #create the directory
      cd $$iisAppsPath
      $iisAppPath = New-Item $iisQueryGridName -type directory -PhysicalPath $directoryPath
      $iisAppPath | Set-ItemProperty -Name "applicationPool" -Value $iisAppPoolName
      cd ..
    }

    #check if site was started and start if not
    if(!((Get-WebsiteState -Name $iisAppName).Value -eq "Started")) {
      Start-Website $iisAppName
    } 
  EOH
end

powershell "Check web app and app pool states for Workflows" do
  code <<-EOH
    Import-Module WebAdministration
    $iisAppPoolName = "QueryGridAppPool"
    $iisAppName = "QueryGrid"

    #navigate to the app pools root
    cd IIS:/AppPools/

    #check if the app pool exists and started
    if (Test-Path $iisAppPoolName -pathType container)
    {
      if(!((Get-WebAppPoolState -Name $iisAppPoolName).Value -eq "Started"))
      {
        Start-WebAppPool $iisAppPoolName
      }        
    }

    #navigate to the sites root
    cd IIS:/Sites/

    #check if the site exists and started
    if (Test-Path $iisAppName -pathType container)
    {
      if(!((Get-WebsiteState -Name $iisAppName).Value -eq "Started")) {
        Start-Website $iisAppName
      }
    }
  EOH
end
