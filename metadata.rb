name 		 "query-grid-mw"
maintainer       "Novartis"
maintainer_email "sergey.myasnikov@novartis.com"
license          "All rights reserved"
description      "Deploys Query Drid"
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          "0.5.8"


depends "windows"
depends "powershell"
depends "7-zip"
